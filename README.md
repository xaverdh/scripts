# Haskell Scripts

## What it is

Haskell scripts for making my life (and yours?) easier.
All of them can by executed as is, if you have the [nix package manager][nix].
Otherwise you can run them as
```sh
runghc SCRIPT ARGS..
```
..if you have ghc and other relevant packages installed.

## Listing

# ghc-with.hs

Thin wrapper that calls [nix][nix] to get a ghc(i) into scope with a list of packages available.

Requires:
* packages: nix
* haskell packages: optparse-applicative

# ffxrecord.hs

Haskell script to record your X screen.

Requires:
* packages: ffmpeg
* haskell packages: X11, optparse-applicative

# wifi2qr.hs

Haskell script to generate a qr code for your wifi.

Requires:
* packages: qrencode
* haskell packages: process optparse-applicative bytestring

# qr2wifi.hs

Haskell script to read wifi login data from qr codes.

Requires:
* packages: zbar
* haskell packages: process parsers bytestring attoparsec optparse-applicative data-default

# mpris-watch-mp.hs

Haskell script to watch for added / removed media players (e.g. a remote bluetooth headset) via mpris messages over dbus.

Requires:
* haskell packages: extra dbus 

## Legal
    These programs are free software: you can redistribute them and/or modify
    them under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.
    See LICENSE for more details.

[nix]: https://nixos.org/nix/
