#!/usr/bin/env nix-shell
#!nix-shell -I nixpkgs=flake:nixpkgs -i runghc -p ffmpeg -p "haskellPackages.ghcWithPackages (ps: with ps; [optparse-applicative X11])"

import qualified Graphics.X11.Xlib.Display as XD
import qualified Graphics.X11.Xlib.Types as XT

import System.Environment (lookupEnv)
import System.Process (callProcess)
import Data.Maybe (fromMaybe)
import Text.Read (readMaybe)
import Numeric.Natural
import Control.Monad
import Control.Applicative

import Options.Applicative (Parser,ParserInfo)
import Options.Applicative.Builder (InfoMod,help,long,short,value,strOption,auto,option,info,defaultPrefs,showDefault)
import Options.Applicative.Extra (helper,customExecParser,prefDisambiguate)


main :: IO ()
main = join $ customExecParser prefs pinfo
  where
    pinfo = hinfo program mempty
    prefs = defaultPrefs
      { prefDisambiguate = True }


hinfo :: Parser a -> InfoMod a -> ParserInfo a
hinfo p = info (helper <*> p)


program :: Parser (IO ())
program =  pure record
  <*> strOption
      ( long "out" <> short 'o'
      <> help "Where to put the generated video file. \
              \For example: -o /tmp/out.mpg" )
  <*> option auto
      ( long "rate" <> short 'r'
      <> value 25
      <> help "Frame rate to record at."
      <> showDefault )
  <*> optional
      ( strOption
        ( long "display"
        <> help "The X display and screen to use. Defaults to $DISPLAY \
              \or :0.0 if DISPLAY is not set." ) )



record :: FilePath -> Natural -> Maybe String -> IO ()
record outpath rate mbDisplayString = do
  mbDisplayVar <- lookupEnv "DISPLAY"
  
  let Just displayString = mbDisplayString <|> mbDisplayVar <|> Just ":0.0"

  let (displayPart,mbScreen) = fromDisplayString displayString
  let screen = fromMaybe 0 mbScreen
  
  d <- XD.openDisplay displayPart
  let w = XD.displayWidth d screen
  let h = XD.displayHeight d screen
  XD.closeDisplay d
  callProcess "ffmpeg"
   [ "-f", "x11grab"
   , "-s", show w <> "x" <> show h
   , "-framerate", show rate
   , "-i", displayString
   , outpath ]


fromDisplayString :: String -> (String,Maybe XT.ScreenNumber)
fromDisplayString ds = case parseDisplayString ds of
  Just res -> res
  Nothing -> error $ ds <> " is an invalid display value" 


parseDisplayString :: String -> Maybe (String,Maybe XT.ScreenNumber)
parseDisplayString ds = work (reverse ds) ""
  where
    work [] [] = Nothing
    work [d] sbits = Just ( d:sbits, Nothing )
    work (d:ds) sbits = case d of
      ':' -> Just ( reverse ds, Nothing )
      '.' -> case readMaybe sbits of
        Just screen -> Just ( reverse ds, Just screen )
        Nothing -> Nothing
      _ -> work ds (d:sbits)

 
