#!/usr/bin/env nix-shell
#!nix-shell -I nixpkgs=flake:nixpkgs -i runghc -p "haskellPackages.ghcWithPackages (ps: with ps; [ extra dbus containers ])"
{-# language ScopedTypeVariables, ViewPatterns #-}
import DBus
import DBus.Client
import DBus.Internal.Types

import Control.Monad
import Control.Monad.Extra

import Control.Concurrent
import Control.Concurrent.MVar
import System.IO
import Data.List (stripPrefix)

import qualified Data.Map as M

extractConnectedStatus :: [Variant] -> Maybe Bool
extractConnectedStatus vs = case vs of
  ( ( fromVariant -> Just "org.bluez.MediaControl1" )
    : ( fromVariant -> Just (dict :: M.Map String Variant) )
    : _ ) -> M.lookup "Connected" dict >>= fromVariant
  _ -> Nothing

handleSignal :: MVar Bool -> Signal -> IO ()
handleSignal mvar s = case extractConnectedStatus $ signalBody s of
  Just con -> void $ tryPutMVar mvar con
  _ -> pure ()

matchBluezPC = matchAny
  { matchMember = Just $ memberName_ "PropertiesChanged"
  , matchPathNamespace = Just $ fromElements [ "org", "bluez" ] }

waitForDisconnect mvar = do
  con <- takeMVar mvar
  when con $ waitForDisconnect mvar

main = do
  mvar <- newEmptyMVar
  client <- connectSystem
  h <- addMatch client matchBluezPC $ handleSignal mvar
  waitForDisconnect mvar
  removeMatch client h
  disconnect client


