#!/usr/bin/env nix-shell
#!nix-shell -I nixpkgs=flake:nixpkgs -i runghc -p qrencode -p feh -p "haskellPackages.ghcWithPackages (ps: with ps; [ process optparse-applicative bytestring hex ini ])"

{-# language LambdaCase, OverloadedStrings #-}

import System.Process
import System.IO (Handle,hPutStr,stdout)

import Control.Monad
import Control.Applicative
import qualified Data.ByteString as B
import qualified Data.List as L
import qualified Data.Text as T

import Options.Applicative (Parser,ParserInfo)
import Options.Applicative.Builder (InfoMod,help,long,short,value,switch,auto,option,strOption,info,defaultPrefs,metavar,strArgument,subparser,command)
import Options.Applicative.Extra (helper,customExecParser,prefDisambiguate)

import Data.Hex (hex)
import Data.Char (toLower,isAlphaNum)
import Data.Ini (readIniFile,lookupValue)

main :: IO ()
main = join $ customExecParser prefs pinfo
  where
    pinfo = hinfo program mempty
    prefs = defaultPrefs
      { prefDisambiguate = True }

hinfo :: Parser a -> InfoMod a -> ParserInfo a
hinfo p = info (helper <*> p)

program :: Parser (IO ())
program = subparser
  ( command "manual" (mkCommand manualP)
  <> command "iwd" (mkCommand $ pure iwd <*> connArgument)
  <> command "networkmanager" (mkCommand $ pure networkManager <*> connArgument) )
  where
    mkCommand p = hinfo (p <*> raw) mempty
    
    raw = switch
      ( long "raw" <> short 'r'
      <> help "Output the data as png image on stdout, instead of displaying the qr code." )

    connArgument = strArgument
      ( metavar "CONNECTION"
      <> help "The connection to export." )

manualP = pure wifi2qr
  <*> strOption
    ( long "ssid" <> short 's'
    <> help "The ssid of the network to export.")
  <*> strOption
    ( long "passphrase" <> short 'p'
    <> help "The passphrase of the network." )

iwd :: String -> Bool -> IO ()
iwd name raw = do
  pass <- getPasswordIwd ssid
  wifi2qr ssid pass raw
  where
    ssid = getSSIDIwd name

networkManager :: String -> Bool -> IO ()
networkManager name raw = do
  ssid <- getSSIDNetworkManager name
  pass <- getPasswordNetworkManager name
  wifi2qr ssid pass raw


wifi2qr :: String -> String -> Bool -> IO ()
wifi2qr ssid pass raw = do
  h <- qrEncode $ createWifi ssid pass
  case raw of
    True -> B.hGetContents h >>= B.hPutStr stdout
    False -> do
      ph <- display h
      void $ waitForProcess ph


display :: Handle -> IO ProcessHandle
display h = do
  (_,_,_,ph) <- createProcess $
    ( proc "feh" [ "-" ] ) { std_in = UseHandle h }
  pure ph


qrEncode :: String -> IO Handle
qrEncode input = do
  (Just hin,Just hout,_,_) <- createProcess $
    ( proc "qrencode"
      [ "-l", "H"
      , "-o", "-" ] )
    { std_in = CreatePipe
    , std_out = CreatePipe }
  hPutStr hin input
  pure hout


getSSIDIwd :: String -> String
getSSIDIwd name
  | all valid name = name
  | otherwise = "=" <> (toLower <$> hex name)
  where
    valid = liftA2 (||) isAlphaNum $ flip L.elem [ '-', ' ' ,'_' ]

getPasswordIwd :: String -> IO String
getPasswordIwd ssid = do
  ini <- either (const invalid) id <$> readIniFile path
  pure $ either (const noparse) T.unpack $ lookupValue "Security" "Passphrase" ini
  where
    invalid = error $ "invalid ini file: " <> path
    noparse = error $ "could not extract passphrase from: " <> path
    path = "/var/lib/iwd/" <> ssid <> ".psk"


getSSIDNetworkManager :: String -> IO String
getSSIDNetworkManager name = stripNewline
  <$> readProcess "nmcli"
    [ "-g", "802-11-wireless.ssid"
    , "connection", "show", name ] ""


getPasswordNetworkManager :: String -> IO String
getPasswordNetworkManager name = stripNewline
  <$> readProcess "nmcli"
    [ "-s"
    , "-g", "802-11-wireless-security.psk"
    , "connection", "show", name ] ""


-- | Strip trailing newline
stripNewline :: String -> String
stripNewline [] = []
stripNewline [x]
  | x == '\n' = []
  | True = [x]
stripNewline (x:xs) = x : stripNewline xs


createWifi :: String -> String -> String
createWifi ssid pass =
  "WIFI:S:" <> quote ssid <> ";T:WPA;P:" <> quote pass <> ";;"

quote :: String -> String
quote [] = []
quote (c:s)
  | c `L.elem` escape = '\\' : c : quote s
  | True = c : quote s
  where
    escape = ['"', ';', ':', ',', '\\' ] :: [Char]


  


