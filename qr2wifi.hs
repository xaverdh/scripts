#!/usr/bin/env nix-shell
#!nix-shell -I nixpkgs=flake:nixpkgs -i runghc -p zbar -p "haskellPackages.ghcWithPackages (ps: with ps; [ process parsers bytestring attoparsec optparse-applicative data-default ])"

{-# language LambdaCase, ScopedTypeVariables, OverloadedStrings #-}

import System.Process
import System.IO (Handle)

import Control.Monad
import Control.Applicative
import Control.Concurrent (threadDelay)
import Data.Functor
import Data.Bifunctor


import qualified System.IO.Error as IOE

import qualified Data.Attoparsec.ByteString as AP
import qualified Data.ByteString as B

import Text.Parser.Char
import Text.Parser.Combinators

-- import qualified Text.PrettyPrint.ANSI.Leijen.Internal as PP

import qualified Options.Applicative as OA
import qualified Options.Applicative.Builder as OB
import Options.Applicative.Builder (help,long,short,value,strOption)
import Options.Applicative.Extra (helper,customExecParser,prefDisambiguate)

{-
 - TODO:
 -  * Test connection saving mode extensively; probably broken atm.
 - -}



{-
 - Cli Code
 - -}

hdr = "qr2wifi: Scan qr code to log into wifi."
introduction = "This program allows to scan a qr code\
  \ containing wifi login data and connect to the\
  \ corresponing wifi / take other actions.\
  \ The default action is to try to print the connection details on stdout.\
  \ The input can be an image file given on the command line;\
  \ otherwise the program will try to use your camera to scan a code."

main :: IO ()
main = join $ customExecParser prefs pinfo
  where
    pinfo = hinfo progP $ OB.header hdr <> OB.progDesc introduction
    prefs = OB.defaultPrefs
      { prefDisambiguate = True }

hinfo :: OA.Parser a -> OB.InfoMod a -> OA.ParserInfo a
hinfo p = OB.info (helper <*> p)

progP :: OA.Parser (IO ())
progP = pure prog
 <*> ( fromImage  <|> fromCamera )
 <*> ( OB.subparser backendP
     <|> pure PrintBackend )


backendP :: OB.Mod OB.CommandFields Backend
backendP =
  cmd "network-manager" ( NetworkManagerBackend <$> saveOnlyP )
  <> cmd "iwd" ( pure $ IwdBackend False )
  <> cmd "print" ( pure PrintBackend )
  <> cmd "debug" ( pure DebugBackend )
  where cmd name p = OB.command name (hinfo p mempty)


saveOnlyP = OB.switch
  ( long "save-only" <> short 's'
  <> help "Don't connect, just save the connection profile." )
 

fromImage :: OA.Parser Frontend
fromImage = pure ImageFrontend
  <*> OB.strOption
    ( long "image" <> short 'i' <> OB.metavar "IMAGE_PATH"
    <> help "Path to an image containing the qr code data." )


fromCamera :: OA.Parser Frontend
fromCamera = pure CameraFrontend
  <*> strOption
      ( long "device" <> short 'd'
      <> value "/dev/video0" <> OB.showDefault
      <> OB.metavar "VIDEO_DEVICE"
      <> help "Video device to use." )
  <*> optional
      ( strOption
        ( long "resolution" <> short 'r'
        <> OB.metavar "WIDTHxHEIGHT"
        <> help "Request resolution from video driver\
                \, e.g. --res 1920x1080." ) )

data Frontend = 
  CameraFrontend FilePath (Maybe String)
  | ImageFrontend FilePath

data Backend =
  PrintBackend
  | DebugBackend
  | NetworkManagerBackend Bool
  | IwdBackend Bool



{-
 - Main Code
 - -}


prog :: Frontend -> Backend -> IO ()


prog (ImageFrontend imagePath) backend = do
  (h,ph) <- zbarimg imagePath
  parseFromHandle h >>= \case
    Right wifiData -> handleWifiData backend wifiData
    Left err -> putStr err
  void $ waitForProcess ph

 
prog (CameraFrontend device mbResolution) backend = do
  (h,ph) <- zbarcam device mbResolution
  parseFromHandle h >>= \case
    Right wifiData -> handleWifiData backend wifiData
    Left err -> putStr err
  terminateProcess ph



handleWifiData :: Backend -> WifiData -> IO ()
handleWifiData backend = case backend of
  NetworkManagerBackend True -> saveWifiNetworkManager
  NetworkManagerBackend False -> conWifiNetworkManager
  IwdBackend True -> saveWifiIwd
  IwdBackend False -> conWifiIwd
  DebugBackend -> print
  PrintBackend -> putStrLn . prettyWifiData


conWifiNetworkManager :: WifiData -> IO ()
conWifiNetworkManager
  (WifiData (SSID ssid) _ (Password pass) visib) =
  callProcess "nmcli" $
    [ "device", "wifi", "connect", ssid, "password", pass ]
    <> (case visib of
          Hidden -> [ "hidden", "yes" ]
          Visible -> [ "hidden" , "no" ]
          Unknown -> [])

conWifiIwd :: WifiData -> IO ()
conWifiIwd
  (WifiData (SSID ssid) _ (Password pass) visib) = do
  callProcess "iwctl" $
    [ "station", device, "scan" ]
  threadDelay (3 * 10^6)
  callProcess "iwctl" $
    [ "--passphrase", pass, "station", device, connect, ssid ]
  where
    device = "wlan0"
    connect = case visib of
      Hidden -> "connect-hidden"
      _ -> "connect"

saveWifiIwd :: WifiData -> IO ()
saveWifiIwd = error "not implemented (use conWifiIwd)"

saveWifiNetworkManager :: WifiData -> IO ()
saveWifiNetworkManager
  (WifiData (SSID ssid) crypto (Password pass) visib) =
  callProcess "nmcli" $
    [ "connection", "add"
    , "ifname", "wlp1s0"
    , "type", "wifi"
    , "ssid", ssid
    , "802-11-wireless-security.psk", pass ]
    <> fmtVisib
    <> fmtCrypto
  where
    fmtVisib = case visib of
      Hidden -> [ "hidden", "TRUE" ]
      Visible -> [ "hidden", "FALSE" ]
      Unknown -> []
    fmtCrypto = case crypto of
      CryptoUnknown -> []
      CryptoNone -> []
      CryptoWpa -> [ "key-mgmt", "wpa-psk" ]
      CryptoWep -> [ "key-mgmt", "none" ]


 
zbarimg :: FilePath -> IO (Handle,ProcessHandle)
zbarimg path = do
  (_,Just hout,_,ph) <- createProcess $ 
    ( proc "zbarimg" ["--oneshot","--raw",path] )
    { std_out = CreatePipe }
  pure (hout,ph)


zbarcam :: String -> Maybe String -> IO (Handle,ProcessHandle)
zbarcam dev mbRes = do
  (_,Just hout,_,ph) <- createProcess $ 
    ( proc "zbarcam" args )
    { std_out = CreatePipe }
  pure (hout,ph)
  where
    args = "--oneshot" : "--raw" : dev : maybe [] (\res -> [ "--prescale=" <> res ]) mbRes




{-
 - Data Definitions
 - -}

data Crypto = CryptoWpa | CryptoWep | CryptoNone | CryptoUnknown
  deriving (Eq,Ord,Show)

data Visibility = Hidden | Visible | Unknown
  deriving (Eq,Ord,Show)

newtype Password = Password String
  deriving (Eq,Ord,Show)

newtype SSID = SSID String
  deriving (Eq,Ord,Show)

data WifiData = WifiData SSID Crypto Password Visibility
  deriving (Eq,Ord,Show)


prettyWifiData :: WifiData -> String
prettyWifiData wifiData = join . map (<>"\n") $
  [ "ssid: " <> ssid
  , "password: " <> pass ]
  <> maybe [] (pure . ("encryption: "<>)) (prettyCrypto crypto)
  <> maybe [] (pure . ("hidden?: "<>))  (prettyVisibility visib)
  where
    WifiData (SSID ssid) crypto (Password pass) visib = wifiData

prettyCrypto :: Crypto -> Maybe String
prettyCrypto = \case
  CryptoUnknown -> Nothing
  enc -> Just $ case enc of
    CryptoWpa -> "wpa"
    CryptoWep -> "wep"
    CryptoNone -> "none"

prettyVisibility :: Visibility -> Maybe String
prettyVisibility = \case
  Unknown -> Nothing
  v -> Just $ case v of
    Hidden -> "hidden"
    Visible -> "visible"


{-
 - Parser Code
 - -}

parseCode :: B.ByteString -> Either String WifiData
parseCode = AP.parseOnly wifiP

parseFromHandle :: Handle -> IO (Either String WifiData)
parseFromHandle h =
  either
    (\e -> Left $ "IO error occured: " <> show e)
    (first ("Parse Error occured: " <>) . parseCode)
    <$> IOE.tryIOError (B.hGetContents h)

item s p = try (string s *> char ':') *> p <* char ';'

-- Format is:  WIFI:S:<SSID>;T:<WPA|WEP|>;P:<password>;H:<true|false|>;;
-- We allow omitting the H and T entries.
wifiP :: AP.Parser WifiData
wifiP = item "WIFI"
  ( pure WifiData
  <*> ssidP
  <*> (cryptoP <|> pure CryptoUnknown)
  <*> passP
  <*> (visibilityP <|> pure Unknown) )


escapedP = many $ noneOf esc <|> ( char '\\' *> oneOf esc )
  where esc = "\";:,\\" 

ssidP = SSID <$> item "S" escapedP

cryptoP = item "T" $
  ( string "WPA" $> CryptoWpa )
  <|> ( string "WEP" $> CryptoWep )
  <|> pure CryptoNone

passP = Password <$> item "P" escapedP

visibilityP = item "H" $
  ( string "true" $> Hidden )
  <|> ( string "false" $> Visible )
  <|> pure Unknown


