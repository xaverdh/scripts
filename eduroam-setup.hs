#!/usr/bin/env nix-shell
#!nix-shell -I nixpkgs=flake:nixpkgs -i runghc -p "haskellPackages.ghcWithPackages (ps: with ps; [ optparse-applicative haskeline ])"

import qualified System.Directory as D
import System.Console.Haskeline
import System.Process

import qualified System.IO as IO
import qualified Data.Map as M
import qualified Data.List as L
import Control.Monad.Trans.Maybe
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Catch
import Control.Applicative
import Data.Functor.Identity
import Data.Maybe

import Options.Applicative (Parser,ParserInfo)
import Options.Applicative.Builder (InfoMod,help,long,short,value,flag',strOption,auto,option,info,metavar,showDefault,switch)
import qualified Options.Applicative.Builder as OAB
import Options.Applicative.Extra (helper,customExecParser,prefDisambiguate)

startsWith :: Eq a => [a] -> [a] -> Bool
startsWith prefix = isJust . L.stripPrefix prefix

main :: IO ()
main = join $ customExecParser prefs pinfo
  where
    pinfo = hinfo program mempty
    prefs = OAB.defaultPrefs
      { prefDisambiguate = True }

hinfo :: Parser a -> InfoMod a -> ParserInfo a
hinfo p = info (helper <*> p)

data Output = Stdout | Networkmanager | OutPath String

data ConfigD f = Config
  { configInterface :: f String
  , configDomain :: f String
  , configDomainMask :: f String
  , configCertPath :: f String
  , configUsername :: f String
  , configPassword :: f String }

type Config = ConfigD Identity

emptyConfig = Config Nothing Nothing Nothing Nothing Nothing Nothing


program :: Parser (IO ())
program = pure eduroamSetup
  <*> ( flag' Stdout
        ( long "stdout"
        <> help "Output the generated configuration to stdout." )
      <|> (flag' Networkmanager)
        ( long "networkmanager"
        <> help "Add the connection to networkmanager." )
      <|> ( fmap OutPath . strOption )
        ( long "out" <> short 'o'
        <> metavar "PATH"
        <> value "/var/lib/iwd/eduroam.8021x" <> showDefault
        <> help "Path to write the generated configuration to." ) )
  <*> ( pure Config <*> ifaceP <*> domainP <*> domainMaskP
       <*> certPathP <*> unameP <*> passP )
  where
    domainP = ( optional . strOption )
      ( long "domain" <> short 'd'
      <> metavar "DOMAIN"
      <> help "The domain of your home institution." )
    domainMaskP =( optional . strOption )
      ( long "domain-mask" <> short 'm'
      <> metavar "DOMAIN_MASK"
      <> help "" )
    certPathP = ( optional . strOption )
      ( long "certpath" <> short 'c'
      <> metavar "PATH"
      <> help "Path to server side certificate." )
    ifaceP = ( optional . strOption )
      ( long "iface" <> short 'i'
      <> metavar "INTERFACE"
      <> help "Wifi interface name." )
    unameP = ( optional . strOption )
      ( long "username" <> short 'u'
      <> metavar "USERNAME"
      <> help "Username at your institution." )
    passP = ( optional . strOption )
      ( long "password" <> short 'p'
      <> metavar "PASSWORD"
      <> help "Password at your institution." )


eduroamSetup :: Output -> ConfigD Maybe -> IO ()
eduroamSetup output config = case output of
  Networkmanager -> do
    ifaces <- filter isWlan <$> D.listDirectory "/sys/class/net"
    Just conf <- assemble $ listToMaybe ifaces
    callNetworkmanager conf
  OutPath path -> IO.withFile path IO.WriteMode $ \h -> do
    Just conf <- assemble $ Just ""
    IO.hPutStr h $ createIwdSnippet conf
  Stdout -> do
    Just conf <- assemble $ Just ""
    IO.putStr $ createIwdSnippet conf
  where
    assemble = runInputT defaultSettings . runMaybeT
      . assembleConfig config
    isWlan = startsWith "wl"


callNetworkmanager :: Config -> IO ()
callNetworkmanager conf = callProcess "nmcli"
  [ "con", "add"
  , "type", "wifi"
  , "connection.id", "eduroam"
  , "802-1x.eap", "ttls"
  , "802-1x.identity", get configUsername
  , "802-1x.anonymous-identity", get configDomain
  , "802-1x.ca-cert", get configCertPath
  , "802-1x.altsubject-matches", "[" <> get configDomainMask <> "]"
  , "802-1x.phase2-auth", "pap"
  , "802-11-wireless.ssid", "eduroam"
  , "802-11-wireless.mode", "infrastructure"
  , "802-11-wireless-security.key-mgmt", "wpa-eap"
  , "ifname", get configInterface ]
  where get f = runIdentity $ f conf


assembleConfig :: (MonadIO m,MonadMask m)
  => ConfigD Maybe -> Maybe String -> MaybeT (InputT m) Config
assembleConfig config mbiface = do
  domain <- knowOrAsk configDomain askDomain
  domainMask <- knowOrAsk configDomainMask askDomainMask
  certpath <- knowOrAsk configCertPath askCertPath
  iface <- case mbiface of
    Nothing -> knowOrAsk configInterface askInterface
    Just i -> pure i
  uname <- knowOrAsk configUsername askUsername
  pass <- knowOrAsk configPassword askPassword
  pure $ Config
    { configInterface = Identity iface
    , configDomain = Identity $ "eduroam@" <> domain
    , configDomainMask = Identity domainMask
    , configCertPath = Identity certpath
    , configUsername = Identity $ uname <> "@" <> domain
    , configPassword = Identity pass }
  where
    knowOrAsk f ask = MaybeT $ maybe ask (pure . Just) (f config)
    askDomain = getInputLineWithInitial "domain: "
      ("uni-augsburg.de","")
    askDomainMask = getInputLineWithInitial "domain mask: "
      ("radius.rz.uni-augsburg.de","")
    askCertPath = getInputLineWithInitial "path to certificate: "
      ("/etc/ssl/certs/ca-certificates.crt","")
    askInterface = getInputLineWithInitial "wifi interface name: "
      ("wlan0","")
    askUsername = getInputLine "username (without domain): "
    askPassword = getPassword (Just '*') "password: "

createIwdSnippet :: Config -> String
createIwdSnippet conf = mkIni $ M.fromList
  [ "Security" ~> M.fromList
    [ "EAP-Method" ~> "TTLS"
    , "EAP-TTLS-Phase2-Method" ~> "Tunneled-PAP"
    , "EAP-Identity" ~> get configDomain
    , "EAP-TTLS-CACert" ~> get configCertPath
    , "EAP-TTLS-ServerDomainMask" ~> get configDomainMask
    , "EAP-TTLS-Phase2-Identity" ~> get configUsername
    , "EAP-TTLS-Phase2-Password" ~> get configPassword ]
  , "Settings" ~> M.fromList
    [ "AutoConnect" ~> "true" ] ]
  where
    get f = runIdentity $ f conf
    a ~> b = (a,b)

mkIni :: M.Map String (M.Map String String) -> String
mkIni =
  let mkSection = M.foldMapWithKey (\k v -> k <> "=" <> v <> "\n")
   in M.foldMapWithKey (\k vs -> "[" <> k <> "]\n" <> mkSection vs )

