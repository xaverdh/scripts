#!/usr/bin/env nix-shell
#!nix-shell -I nixpkgs=flake:nixpkgs -i runghc -p "haskellPackages.ghcWithPackages (ps: with ps; [ http-conduit optparse-applicative ])"

{-# language OverloadedStrings, LambdaCase #-}

import Network.HTTP.Simple
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC
import qualified Data.Char as C
import qualified System.IO as IO
import Data.Maybe
import Control.Monad (join)
import Control.Applicative

import Options.Applicative (Parser,ParserInfo)
import Options.Applicative.Builder (InfoMod,help,long,short,value,strOption,auto,option,info,defaultPrefs,showDefaultWith,maybeReader,metavar,argument)
import Options.Applicative.Extra (helper,customExecParser,prefDisambiguate)

main :: IO ()
main = join $ customExecParser prefs pinfo
   where
     pinfo = hinfo program mempty
     prefs = defaultPrefs { prefDisambiguate = True }

hinfo :: Parser a -> InfoMod a -> ParserInfo a
hinfo p = info (helper <*> p)

program :: Parser (IO ())
program = pure nixpkgsLookup
  <*> many
    ( argument (maybeReader mkChannel)
      ( metavar "CHANNEL"
      <> help "channel to look up (e.g. nixos-unstable)" ) )


newtype Channel = Channel { getChanString :: String }

mkChannel :: String -> Maybe Channel
mkChannel s = if s `elem` chans then Just (Channel s) else Nothing
  where
    chans =
     [ "nixpkgs-unstable"
     , "nixos-unstable"
     , "nixos-unstable-small" ]

req :: Channel -> IO Request
req chan = parseRequest $
  "HEAD https://nixos.org/channels/"
  <> getChanString chan
  <> "/git-revision"

nixpkgsLookup :: [] Channel -> IO ()
nixpkgsLookup [] = pure ()
nixpkgsLookup (chan:chans) = do
  res <- httpBS =<< req chan
  printResult chan (extractRev res) (extractDate res)
  nixpkgsLookup chans

extractDate :: Response a -> Maybe B.ByteString
extractDate = lookup "Last-Modified" . getResponseHeaders

extractRev :: Response B.ByteString -> B.ByteString
extractRev = getResponseBody

printResult :: Channel -> B.ByteString -> Maybe B.ByteString -> IO ()
printResult chan rev maybeDate = do
  putStrLn $ getChanString chan
  B.putStr res
  where
    res = sanitize rev <> "\n" <> sanitize date <> "\n"
    date = fromMaybe "could not extract date" maybeDate

sanitize :: B.ByteString -> B.ByteString
sanitize = BC.filter C.isPrint



