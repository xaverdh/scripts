#!/usr/bin/env nix-shell
#!nix-shell -I nixpkgs=flake:nixpkgs -i runghc -p "haskellPackages.ghcWithPackages (ps: with ps; [ process optparse-applicative ])"

import System.Process (callProcess)
import Text.Printf (printf)

import Control.Monad
import Control.Applicative

import Options.Applicative (Parser,ParserInfo)
import Options.Applicative.Builder (InfoMod,help,long,short,value,switch,strOption,info,defaultPrefs,metavar,strArgument)
import Options.Applicative.Extra (helper,customExecParser,prefDisambiguate)

main :: IO ()
main = join $ customExecParser prefs pinfo
  where
    pinfo = hinfo program mempty
    prefs = defaultPrefs
      { prefDisambiguate = True }

hinfo :: Parser a -> InfoMod a -> ParserInfo a
hinfo p = info (helper <*> p)

program :: Parser (IO ())
program =  pure ghcWith
  <*> switch
      ( long "interactive" <> short 'i'
      <> help "Run ghci instead of a shell with ghc." )
  <*> ( optional . strOption )
        ( long "version" <> short 'v'
        <> help "The ghc version to use, e.g. 92 for the most recent ghc92*" )
  <*> many
    ( strArgument
      ( metavar "PACKAGE(S)"
      <> help "The package(s) to make available for the ghc." ) )


ghcWith :: Bool -> Maybe String -> [String] -> IO ()
ghcWith interactive mVersion packages = callProcess "nix"
  $ [ "shell", "--impure", "--expr", expr ]
    <> ( if interactive then [ "-c", "ghci" ] else [] )
  where
    expr = printf "let pkgs = import (builtins.getFlake \"nixpkgs\") {}; in pkgs.%s.ghcWithPackages (ps: with ps; [ %s ])" packageSet $ unwords packages
    packageSet = maybe
      "haskellPackages"
      (printf "haskell.packages.ghc%s")
      mVersion
